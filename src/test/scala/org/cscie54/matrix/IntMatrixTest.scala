package org.cscie54.matrix

import org.cscie54.shared.BasicTestKit

/**
 * Test IntMatrix class with simple test cases
 * without VectorCPU
 *
 */
class IntMatrixTest extends BasicTestKit {

  "IntMatrix.equals" should "be equal to another IntMatrix" in {
    // Integer matrix, size 2x3, filled with 4
    val im1a = new IntMatrix(2, 3, 4)

    // Same as above
    val im1b = new IntMatrix(2, 3, 4)

    assert(im1a.equals(im1b))
  }

  it should "not be equal to a matrix with different values" in {
    // Integer matrix, size 4x4, filled with 1
    val im2 = new IntMatrix(4, 4, 1)

    // Same dimensions as above, but different values
    val im3 = new IntMatrix(4, 4, 2)

    assert(!im2.equals(im3))
  }

  it should "expect us to catch an exception for ops on matrices with different dimensions" in {
    // Integer matrix, size 2x5, filled with 0
    val im4 = new IntMatrix(2, 5, 0)

    // Integer matrix, size 3x5, filled with 0
    val im5 = new IntMatrix(3, 5, 0)

    intercept[IllegalArgumentException] {
      println(im4.equals(im5))
    }
  }

  "IntMatrix.{+,add}" should "add int matrices" in {
    // Integer matrix, size 4x4, filled with 1
    val im2 = new IntMatrix(4, 4, 1)

    // Same dimensions as above, but different values
    val im3 = new IntMatrix(4, 4, 2)

    // Expected matrix representing sum
    val imSumExpected: IntMatrix = new IntMatrix(4, 4, 3)

    // Add two matrices together to create new one using "add" method
    val imSum = add(im2, im3)

    // Make sure matrix with sum matched expected values
    assert(imSum.equals(imSumExpected))
    //imSum.printMatrix()

    // Add two matrices together to create new one using "+" operator
    //println("Using + operator")
    val imSum2: IntMatrix = im2 + im3
    assert(imSum2.equals(imSumExpected))
    //imSum2.printMatrix()
  }

  "IntMatrix.sum" should "add two matrices" in {
    // Integer matrix, size 2x2, filled with 1
    val im6 = new IntMatrix(2, 2, 1)

    // Integer matrix, size 2x2, filled with 2
    val im7 = new IntMatrix(2, 2, 2)

    // Expected matrix representing sum
    val im8: IntMatrix = new IntMatrix(2, 2, 3)

    // Compute sum and print
    val im9: IntMatrix = im6.sum(im7)

    // Make sure matrix with sum matched expected values
    assert(im9.equals(im8))

    // Uncomment to see printouts
    //im8.printMatrix()

    //println("")
    //val im99: Array[Array[Int]] = Array(Array(1, 2, 3), Array(4, 5, 6))
    //printArray2D(im99)
  }

  "IntMatrix.toString" should "print out the matrix" in {
    // Integer matrix, size 2x2, filled with 1
    val im = new IntMatrix(2, 2, 1)
    println(im)
    val expected =
        "\n" +
        "1 | 1 | \n" +
        "1 | 1 | \n"

    im.toString should equal(expected)
  }

}
