package org.cscie54.matrix

/**
 * Matrix, parent type of other matrix types
 *
 *
 */
trait Matrix[A] extends {
  val rows: Int
  val cols: Int
  val get: Array[Array[A]]

  // Syntactic sugar for callers who want to access cell values, same as "get" above
  // Also used here in methods for addressing elements
  def cell = get

  /**
   * Return matrix dimensions as a tuple
   *
   * @return a tuple of the rows and columns in this matrix
   */
  def dim: (Int, Int) = (rows, cols)

  /**
   * See if another matrix has the same dimensions as this one
   *
   * @param other the other matrix whose dimensions we will compare to current matrix
   * @return true if the other matrix has the same dimensions as this one
   */
  def sameSize(other: Matrix[A]): Boolean = {
    other.dim == dim
  }

  /**
   * Check the size of another matrix against this matrix
   *
   * @param other the other matrix to check the size of
   * @throws IllegalArgumentException if the dimension of this matrix is not the same size as the other
   */
  def checkSize(other: Matrix[A]): Unit = {
    if (!sameSize(other)) throw new IllegalArgumentException("Matrices must have the same dimensions")
  }


  /**
   * Return true if other matrix has same values as this one
   * You might need to override this in your implementing class, depending on
   * what you think "equals" means
   *
   * @param other the
   * @return
   */
  def equals(other: Matrix[A]): Boolean = {
    checkSize(other)

    val matrix2 = other.get

    for (r <- 0 to rows-1; c <- 0 to cols-1) {
      if (cell(r)(c) != matrix2(r)(c)) return false
    }

    true
  }


  /**
   * Print this matrix
   *
   */
  def printMatrix(): Unit = {
    println(toString())
  }

  override def toString(): String = {
    val sb = new StringBuilder()
    sb.append("\n")
    for (r <- 0 to rows-1) {
      for (c <- 0 to cols-1) {
        sb.append(cell(r)(c) + " | ")
      }
      sb.append("\n")
    }
    sb.toString()
  }
}
