# Performance Benchmarking 

To contribute your performance benchmarking, please follow these instructions. First, download the project: 

    > git Clone git@github.com:CSCI-E-54/team-one.git
    > cd team-one
    
Next, open src/main/org/cscie54/shared/BasicTestKit.scala and fill in information at top:

```scala
  ///////////// TESTING METRICS: Fill in your info below ////////////////////////

  // 1. ADD A NEW TESTER FOR YOUR COMPUTER
  //  val YourNameHere =  Tester(person = "YourNameHere",
  //                             cores = "YourNumCores",
  //                             processor = "Y.Z GHz Your Processor Name Here" )
  val Myer =      Tester(person= "Myer" ,       cores=  "2" , processor = "2.5 GHz Intel Core i5"  )
  val Harry =     Tester(person= "Harry",       cores=  "2" , processor = "2.5 GHz Intel Core i5"  )
  val Ivan =      Tester(person = "Ivan",       cores = "4" , processor = "2.3 Ghz Intel Core i7"  )

  // 2. ADD YOUR TESTER TO THE CURRENT TESTING SESSION
  val curTestSession = TestSession(Myer, date) // replace name here with your own

  // 3. RUN TESTS AND SEND vector-processor-performance-metrics.txt to Myer at myer.nore@gmail.com
  //    OR INSERT THEM INTO COLUMN `A` AT https://goo.gl/j0UuIi BELOW ALL OTHER ENTRIES.

  ///////////// TESTING METRICS: Fill in your info above ////////////////////////
```

After filling in a Tester for your computer and adding that Tester to curTestSession, start sbt in the team-one directory, and use the `test` directive. Send the `vector-processor-performance-metrics.txt` file to Myer at [myer.nore@gmail.com](mailto:myer.nore@gmail.com). Thank you!

See `src/main/org/cscie54/vector/MeasuredPerformanceTests.scala` for the latest list of tests.

```scala
// MATRIX MATH
AddIntMatrixMeasuredPerformanceTest.runTests()
        
// FOLD OPERATION OVER ARRAY
FoldAddIntArrayMeasuredPerformanceTest.runTests()
FoldMultIntArrayMeasuredPerformanceTest.runTests()

// FIND INTEGRAL OF FUNCTION
IntegralMeasuredPerformanceTest.runTests()

// MAPREDUCE WORD COUNT
ShortMapReduceWCProgramMethodMeasuredPerformanceTest.runTests()
ShortMapReduceWCNarrowcastMethodMeasuredPerformanceTest.runTests()
LongMapReduceWCProgramMethodMeasuredPerformanceTest.runTests()
```

# Team One

Project Documentation
* [Performance Metrics](https://docs.google.com/spreadsheets/d/1H9pzWH1w1iIBhf5fghGbZe7bQsEIot6_e-pQRI_38nI/edit?usp=sharing)
* Final Project Presentation Link: http://goo.gl/zCqQcx
* Final Project Writeup: https://github.com/CSCI-E-54/team-one/blob/master/WriteUp/VectorProcessor.pdf

Project Management
* [Team Dashboard](https://docs.google.com/spreadsheets/d/1ksiLcYvFFAmvXxKM0A_3bI90gJ45oONTUcgan6HUNe8/edit)  
* Tasks in Waffle: https://waffle.io/csci-e-54/team-one  

# Testing Conventions

## Naming Conventions

* Extend BasicTestKit in all tests that don't use actors and ActorTestKit in all tests that do use actors
* Use `ClassNameTest` as name for testing files / classes, where ClassName is the class under tests
* For larger integration tests implement the `MeasuredPerformanceTest` abstract class and put it in `MeasuredPerformanceTests`
* For a test of a single method, use `"EntityUnderTest.methodUnderTest" should "expected behavior description"` followed by `it should "expected behavior description 2"` in subsequent tests for that method
* For a test of more than one method, use `"EntityUnderTest.{methodUnderTest1, methodUnderTest2}"` followed by `it` in subsequent tests of that method. Inside the brackets, methods should be alphabetized.

## Tagging
* Integration Tests should be tagged with ITest: `"SomeClass.someMethod" should "itest desc" taggedAs(ITest) in {`
* Load Tests should be tagged with LoadTest: `"SomeClass.someMethod" should "loadtest desc" taggedAs(LoadTest) in {`
* Single threaded benchmark versions of integration tests, use `"SomeClass.someMethod" should "benchmrk desc" taggedAs(Benchmark) in {`

## Test Ordering
* Single method tests should be at the top of the test class, in alphabetical order by Entity.method
* Tests that test more than one method should use `"Entity.{method1, method2}"` as their behavior spec and should be beneath unit tests. They should also be considered for tagging with `ITest` as Integration Tests.
* Unit Tests should test that all of the methods of a given class work as advertised and verify that the system responds to boundary inputs in a predictable way
* Outside of MeasuredPerformanceTest, use BasicTestKit's `printPerf{ // body here }` for the calculation portion of test. This will print the runtime. 
* In MeasuredPerformanceTest, do not use `printPerf{ // body here }`; instead, use the `MeasuredPerformanceTest` methods.
* Name the file for the test after the system under test, e.g. `MasterTest` for a test of `Master`, `RingTest` for a test of `Ring`. 

# Final Project Spec

Due May 11 by 7:40 pm  
Points: 31  

You have all been grouped into teams for the final project.  
Each team has read/write access to their own private repository on GitHub.  
Try to take advantage of the collaboration features GitHub offers, especially related to pull requests.  
Three ideas for the final project have been posted by the teaching staff at:  
https://github.com/CSCI-E-54/handouts/tree/master/assignments/final_project

You are free to work on your own idea, just make sure you submit your proposal to the teaching staff as soon as possible, to get timely feedback.

The last class meeting on May 11th is reserved for final project presentations. Hence the formal deadline for the final project is May 11th at 7:40 pm. At that time, we will revoke your write permissions to the repository and its contents will represent your final project submission. So, Canvas will not be used for submitting, just for grading.

You will have up to 10 minutes to present and 5 minutes maximum for questions. It is up to each team to choose how many team members will present, but a single person presenting is fine.

In your presentation, you should focus on presenting:

- Overall architecture/design of your implementation
- Choice of concurrency abstractions you used and trade-offs you made in selecting them
- Some interesting snippets of code which portray how you solved certain concurrency problems

In your repositories, create a directory called *presentation* and put your slides there.  
The projector system provides HDMI and VGA connections and you are welcome to bring your own laptop.  
If that is not feasible, you will be able to use our laptop for presentation, assuming your presentation does not require anything special pre-installed.

# Hardware Simulator Spec

Develop a software simulation for a vector processor, and
demonstrate it by solving a problem such as numerical
integration of an arbitrary function.

A vector processor is specialized hardware composed of n
independent processors arranged logically in an array.  We
assume each processor has its own memory, and cannot share
objects with others.

    +----------+----------+----------+------
    |  proc 0  |  proc 1  |  proc 2  |  ...
    +----------+----------+----------+------

All processors are identical, except that each is "born"
knowing its index in the array.  It otherwise knows nothing
about its neighbors, or even the size of the array.  For
example, it might be modeled as

  trait Processor {
      def index: Int
      // ...
  }

But there may be other, better ways to model a processor.

In addition to the array of processors, there is a separate
master control program (MCP).  The MCP can broadcast data
to all the processors more or less simultaneously.  The
messages flowing into the processors are all identical,
and the broadcasting mechanism is considered cheap and
efficient.

The broadcast mechanism is the principal means to "program"
the array of processors.  We assume code, in the form of
Scala lambdas, is broadcast.  Getting this right is a bit
tricky, because the processors will need to do different
things based on their individual indices, but they are all
executing identical code.  For example, lambdas might be
of the form (Int) => Unit, where the processor executes the
program by passing in the index it knows.  There may be
other, better signatures for these programs.

Processors emit data, typically the results of computations,
by passing it to their nearest neighbor.  We can imagine a
tiny queue with a maximum capacity of q sits in between
adjacent processors.  The value of q is configurable, but
constant across the array.

For example, if q = 0, then attempts to emit data will block
until the receiver is ready to accept it.  This is like a
simple rendezvous.  More commonly, q = 1, which means that
a processor can emit one datum without blocking.  But any
subsequent attempt to emit will block until the next
processor accepts the first value.

Tuning the q value allows one to manage backpressure, but
for simplicity, we may assume that this value is known at
compile time and does not change as the code runs.

Emission of data is strictly uni-directional.  Processor 0
can only emit to processor 1.  Processor 1 can only accept
from processor 0, and can only emit to processor 2.  And
so on.

However, the MCP can also emit and accept data.  MCP emits
into processor 0, and accepts from processor n-1.  In this
way, the MCP appears to be just another processor in the
array to the other processors, but it is not executing the
same code.

For example, to calculate a "fold", the MCP can seed the
initial processor by emitting a zero value.  Each processor
in turn will accept and emit a value.  Finally, the MCP will
accept the sum from the last processor.  The processors are
all running independently in our simulation, so the MCP
can immediately try to accept the sum, and expect it to
arrive when the computations are complete.

The type of data passed to neighboring processors might be
a Double, or something else.  But for the simulation to
work, such data should not be code, nor should it be mutable.

The third communication mechanism is narrowcasting.  The
MCP, which knows the value of n, is able to transmit data
to an individual processor, by index.  Unlike broadcasting,
this is inefficient, and when done, it arrests all progress
in all the processors.  In other words, to narrowcast to
processor 2, all the processors are blocked while trying
to accept or emit data to their neighbors.  Even those not
adjacent to processor 2 are hamstrung.

The purpose of narrowcasting is to distribute data across
the array.  Code or lambdas are not permitted, but rather
arrays of Ints or Doubles might be expected.  Typically
this would happen at the beginning of the simulation run.

For example, suppose we wanted to calculate the sum of 30,
40, 50, 60, 70, and 80 if we had an n=2 array.  The MCP could
narrowcast [30,40,50] to processor zero, and then narrowcast
[60,70,80] to processor one.

In pseudocode, the MCP might broadcast a lambda like the
following to both processors.  This would essentially fold
over all the data.

    processor =>
        var tally = 0
        for (x <- data) {
            tally += x
        }
        tally += processor.accept()
        processor.emit(tally)

In practice, the signatures of accept and emit would need to
be worked out carefully.  Would Futures or Trys be involved?

    trait Processor {
        def index: Int
        def emit(v: Int): Unit  // Not quite?
        def accept(): Int        // Not quite?
    }

The MCP would then emit(0) to seed the fold, and then accept()
a value back, namely 330.  There are a lot of ways one might
model the MCP itself, but it might share some of the methods
of a Processor.

    trait MCP {
        def n: Int
        def emit(v: Int): Unit  // Not quite?
        def accept(): Int        // Not quite?
        def broadcast(lambda: (Processor) => Unit): Unit
        def narrowcast(index: Int, data: Array[Int]): Unit
    }

But of course, there may be other, better ways to model the
MCP than the above.

The Vector Processor Simulation can demonstrate how to carry
out interesting problems, such as numerical integration.  For
example, suppose you wanted to integrate the cosine function
from zero to pi/4.  This is not a bad first demo, because you
would not need to narrowcast data at all.

With n different processors total, the interval from zero to
pi/4 would be divided into n equal parts.  And then each
processor could compute a portion of the total integral.

Processor zero would compute the integral from zero to pi/(4n).
Simultaneously, processor one would compute the integral from
pi/(4n) to 2pi/(4n), and so on.  The individual sums could
be folded together, and the integral would percolate back to
the MCP.

As another example, instead of using a function like cosine,
which could be broadcast to all the processors, the MCP could
direct the array to compute the integral of arbitrary data.  It
would narrowcast in turn the data for each of the q intervals.

Demonstrating all three kinds of communication: broadcast,
narrowcast, and nearest neighbor, is essential.  And it also
goes without saying, test, test, test.

There are many interesting possibilities here for enhancement.
For example, instead of arbitrary lambdas, perhaps only limited
abstract syntax trees could be broadcast.  Although this is
not trivial, it might make other parts of the simulation easier
to implement.  A DSL for the logic in the array processors could
even be defined.

Also, it will require thought to decide whether Actors or some
other mechanism is appropriate for implementing the processors?
The answer here might depend on whether arbitrary lambdas or
limited abstract syntax trees are used.

There are different communication mechanisms, as well, so might
concurrent collections be appropriate in some cases but not in
others?  Should the simulation rely on objects being immutable,
or should it enforce this by serializing messages?

Moreover, integration is far from the only problem that vector
processors can solve.  There may be other mathematical ones,
like convolution or differentiation.  These are especially
interesting because they may need more than a single fold to
complete.  But different problems like searching or map-reduce
are natural fits, too.

The main idea here is to explore how to use concurrency to
create a useful software simulation.  Real world objects "run"
concurrently all around us.  Contemplating the tradeoffs of
different concurrency patterns to model them is worthy of study
and energy.
