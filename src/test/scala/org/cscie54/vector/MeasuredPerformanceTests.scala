package org.cscie54.vector

import akka.actor.ActorSystem
import org.cscie54.Defaults._
import org.cscie54.VProcsQSize
import org.cscie54.matrix._
import org.cscie54.shared.ActorBasicTestKit

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.io.Source
import scala.language.{postfixOps,higherKinds}

class MeasuredPerformanceTests extends ActorBasicTestKit(ActorSystem("MeasuredPerformanceTest")) {

  /**
   * All tests in this file extend this class to execute measured performance tests.
   * See HELPER TEST OBJECTS section at end of test class.
   * See Defaults.allVProcQSizes in package.scala for a list of the numProcessors and qSizes being tested here
   */
  sealed abstract class MeasuredPerformanceTest {
    def testId: Int

    def entity: String

    def description: String

    val test: TestEntity

    def registerBenchmarkPerformance(f: => Unit) = printPerf(f, test, Benchmark)

    def registerPerformanceTestPerformance(vProcQSize: VProcsQSize, f: => Unit): Unit = printPerf(f, test, vProcQSize)

    def performOnAllVProcQSizes(): Unit = allVProcQSizes.foreach(performanceTest)

    def performanceTest(vProcQSize: VProcsQSize): Unit

    def benchmarkTest(): Unit

    def runTests() = {
      behavior of entity
      it should description taggedAs(ITest, Benchmark, LoadTest) in {
        benchmarkTest()
        performOnAllVProcQSizes()
      }
    }

  }

  // --------------| BEGIN PERFORMANCE / USE CASE TESTS --------->

  // MATRIX MATH
  AddIntMatrixMeasuredPerformanceTest.runTests()

  // FOLD OPERATION OVER ARRAY
  FoldAddIntArrayMeasuredPerformanceTest.runTests()
  FoldMultIntArrayMeasuredPerformanceTest.runTests()

  // FIND INTEGRAL OF FUNCTION
  IntegralMeasuredPerformanceTest.runTests()

  // MAPREDUCE WORD COUNT
  ShortMapReduceWCProgramMethodMeasuredPerformanceTest.runTests()
  ShortMapReduceWCNarrowcastMethodMeasuredPerformanceTest.runTests()
  LongMapReduceWCProgramMethodMeasuredPerformanceTest.runTests()

  // <-------------| END PERFORMANCE / USE CASE TESTS |---------------

  // --------------| BEGIN HELPER TEST OBJECTS UNTIL END OF MASTER PERFORMANCE TEST |------------>

  //////////////////// MATRIX MATH ////////////////////////////////

  object AddIntMatrixMeasuredPerformanceTest extends MeasuredPerformanceTest {
    // Maximum value we will use to fill an array
    // Also the number of matrices we will be adding together
    val nMax = 100000
    // All matrices for this test have these dimensions
    val rows = 4
    val cols = 4
    val entity = "Add Int Matrix"
    val description = s"add [1 to $nMax] ints in $nMax matrices ($rows x $cols)"
    val test: TestEntity = new TestEntity(testId, entity, description)
    // Expected matrix representing sum
    val imSumExpected: IntMatrix = new IntMatrix(rows, cols, (1 to nMax).sum)
    // Fill data array with integer matrices
    val data: Vector[IntMatrix] = {
      var initializedData = Vector[IntMatrix]()
      for (i <- 1 to nMax) initializedData :+= new IntMatrix(rows, cols, i)
      initializedData
    }
    // Matrix filled with zeroes
    val zeroMatrix = new IntMatrix(rows, cols, 0)

    // Create the lambda for summing a group of matrices in one processor
    val lambda: Vector[IntMatrix] => IntMatrix =
      data => {
        var tally = zeroMatrix
        for (x <- data) tally += x
        tally
      }

    def performanceTest(vProcsQSize: VProcsQSize): Unit = {
      val vectorCPU = Master[IntMatrix](vProcsQSize.nProcessors, vProcsQSize.qSize)
      vectorCPU.program(lambda, _ + _, data)
      registerPerformanceTestPerformance(vProcsQSize, {
        vectorCPU.emit(Future(zeroMatrix))
        whenReady(vectorCPU.accept()) { result =>
          assert(result.equals(imSumExpected))
          result.printMatrix()
        }(fiveSecPatience)
      })
    }

    override def benchmarkTest(): Unit = {
      registerBenchmarkPerformance(f = {
        val actual = lambda(data)
        assert(actual.equals(imSumExpected))
      })
    }

    override def testId: Int = 1
  }

  //////////////////// FOLD OPERATION OVER ARRAY ////////////////////////////////

  object FoldIntArrayInfo {
    val upperBound = 10000000
    val data = 1 to upperBound toVector
  }

  object FoldAddIntArrayMeasuredPerformanceTest extends MeasuredPerformanceTest {

    import FoldIntArrayInfo.{data, upperBound}

    val entity = "Add Int Array"
    val description = s"add $upperBound integers in an array, as in reduceLeft"
    val test: TestEntity = new TestEntity(testId,entity, description)
    val lambda: Vector[Int] => Int =
      data => {
        var tally = 0
        for (x <- data) tally += x
        tally
      }

    def performanceTest(vProcsQSize: VProcsQSize): Unit = {
      val cpu = Master[Int](vProcsQSize.nProcessors, vProcsQSize.qSize)
      cpu.program(lambda, _ + _, data)
      registerPerformanceTestPerformance(vProcsQSize, {
        cpu.emit(Future(0))
        whenReady(cpu.accept()) {
          _ should be(data sum)
        }(fiveSecPatience)
      })
    }

    override def benchmarkTest(): Unit = {
      registerBenchmarkPerformance(f = {
        val actual = lambda(data)
        actual should be(data sum)
      })
    }

    override def testId: Int = 2
  }

  object FoldMultIntArrayMeasuredPerformanceTest extends MeasuredPerformanceTest {

    import FoldIntArrayInfo.{data, upperBound}

    val entity = "Multiply Int Array"
    val description = s"multiply $upperBound integers in an array, as in reduceLeft, with load"
    val test: TestEntity = new TestEntity(testId,entity, description)
    val lambda: Vector[Int] => Int =
      data => {
        var tally = 1
        for (x <- data) tally *= x
        tally
      }

    override def performanceTest(vProcsQSize: VProcsQSize): Unit = {
      val cpu = Master[Int](vProcsQSize.nProcessors, vProcsQSize.qSize)
      cpu.program(lambda, _ * _, data)
      registerPerformanceTestPerformance(vProcsQSize, {
        cpu.emit(Future(1))
        whenReady(cpu.accept()) {
          _ should be(data product)
        }(fiveSecPatience)
      })
    }

    override def benchmarkTest(): Unit = {
      registerBenchmarkPerformance(f = {
        val actual = lambda(data)
        actual should be(data product)
      })
    }

    override def testId: Capacity = 3
  }

  //////////////////// FIND INTEGRAL OF FUNCTION ////////////////////////////////

  object IntegralMeasuredPerformanceTest extends MeasuredPerformanceTest {
    val entity = "Integral of a function"
    val description = "find the integral of 1/x from 1 to 10"
    val test: TestEntity = new TestEntity(testId,entity, description)
    val resolution: Int = 500000
    val xLower = 1
    val xUpper = 10

    def f(x: Double) = 1 / x

    val dx: Double = (xUpper - xLower) / resolution.toDouble
    val expected = Math.log(xUpper) - Math.log(xLower)
    val tolerance = .0001
    val fOfX: Vector[Double] => Double = _.map {
      // for each element, calculate f(x), the vertical, and multiply by dx, the horizontal.
      // this is the area of the rectangle for this sliver of the integral.
      f(_) * dx
    }.sum
    // sum all of the rectangles
    // x, x + dx, x + 2dx, ... resolution - dx
    val xVals = Vector.tabulate[Double](resolution)(xLower + dx * _)

    def checkXVals() = {
      xVals.length should be(resolution)
      xVals.head should be(xLower)
      // the last element should be equal to xUpper minus dx, within the tolerance
      Math.abs(xVals(xVals.length - 1) - (xUpper - dx)) should be < tolerance
    }

    def performanceTest(vProcsQSize: VProcsQSize): Unit = {
      val cpu = Master[Double](vProcsQSize.nProcessors, vProcsQSize.qSize)
      cpu.program(fOfX, _ + _, xVals)

      registerPerformanceTestPerformance(vProcsQSize, {
        cpu.emit(Future(0d))
        whenReady(cpu.accept()) { actual =>
          println(s"The integral of 1/x from 1 to 10 was computed at $actual, \ncompared to the expected value $expected")
          Math.abs(actual - expected) should be < tolerance
        }(fiveSecPatience)
      })
    }

    override def benchmarkTest(): Unit = {
      registerBenchmarkPerformance(f = {
        val actual = fOfX(xVals) // essentially xVals.map(dx * 1/_).sum
        println(s"The integral of 1/x from 1 to 10 was computed at $actual, \ncompared to the expected value $expected")
        Math.abs(actual - expected) should be < tolerance
      })
    }

    override def testId: Int = 4
  }

  // END IntegralMeasuredPerformanceTest

  //////////////////// MAPREDUCE WORD COUNT ///////////////////////////

  object ShortMapReduceWCProgramMethodMeasuredPerformanceTest extends MeasuredPerformanceTest {
    override def entity: String = "MapReduce Short Word Count"

    override def description: String = "program method to find word freqs for Sonnet 57 by William Shakespeare"

    override def benchmarkTest(): Unit = MapReduceWordCountInfo.nonConcurrentBenchmark(
      shortShakesLinesWithIndex, shortShakesWordFrequencies, this)

    override def performanceTest(vProcQSize: VProcsQSize): Unit = {
      import MapReduceWordCountInfo.{doMR, mapFn, partition, reduceFn}
      val cpu = Master[Vector[(String, Int)]](vProcQSize.nProcessors, vProcQSize.qSize)
      val groupedData = partition(shortShakesLinesWithIndex, vProcQSize.nProcessors)
      cpu.program(mapFn, reduceFn, groupedData)
      registerPerformanceTestPerformance(vProcQSize, {
        doMR(cpu, shortShakesWordFrequencies)
      })
    }

    // Let my main man Willie Shakes populate the test map
    val shortShakes =
      """Being your slave, what should I do but tend
Upon the hours and times of your desire?
I have no precious time at all to spend,
Nor services to do, till you require.
Nor dare I chide the world without end hour
Whilst I, my sovereign, watch the clock for you,
Nor think the bitterness of absence sour
When you have bid your servant once adieu.
Nor dare I question with my jealous thought
Where you may be, or your affairs suppose,
But, like a sad slave, stay and think of nought
Save, where you are, how happy you make those.
So true a fool is love that in your will,
Though you do anything, he thinks no ill.




      """

    // wordcount courtesy of http://euri.ca/2013/quick-javascript-word-frequency-counter/
    // Case, punctuation and words that only appear once are ignored
    val shortShakesWordFrequencies = Vector(("you", 7), ("your", 5), ("the", 4), ("nor", 4), ("do", 3), ("of", 3), ("but", 2),
      ("have", 2), ("slave", 2), ("to", 2), ("where", 2), ("and", 2), ("dare", 2), ("my", 2), ("think", 2), ("no", 2))

    val shortShakesLinesWithIndex: Vector[(String, Int)] = shortShakes.split("\n").zipWithIndex.toVector

    override val test: TestEntity = new TestEntity(testId,entity, description)

    override def testId: Int = 5
  }

  // END ShortMapReduceWCProgramMethodMeasuredPerformanceTest

  object ShortMapReduceWCNarrowcastMethodMeasuredPerformanceTest extends MeasuredPerformanceTest {
    override def entity: String = "MapReduce Short Word Count"

    override def description: String = "broadcast/narrowcast method to find word freqs for Sonnet 57 by William Shakespeare"

    import ShortMapReduceWCProgramMethodMeasuredPerformanceTest._

    override def benchmarkTest(): Unit = MapReduceWordCountInfo.nonConcurrentBenchmark(
      shortShakesLinesWithIndex, shortShakesWordFrequencies, this)

    override def performanceTest(vProcQSize: VProcsQSize): Unit = {
      import MapReduceWordCountInfo.{doMR, mapFn, reduceFn}
      val by = math.ceil(shortShakesLinesWithIndex.length.toDouble / vProcQSize.nProcessors) toInt
      val linesGroupedByProc: Vector[(Vector[(String, Int)], Int)] =
        shortShakesLinesWithIndex.grouped(by).toVector zip (Stream from 1)
      val vectorCPU = Master[Vector[(String, Int)]](vProcQSize.nProcessors, vProcQSize.qSize)
      for ((data, procIndx) <- linesGroupedByProc) vectorCPU.narrowcast(procIndx, Vector(data))
      /* the client *has* to know about Processor with this scheme */
      val lambda: Processor[Vector[(String, Int)]] => Vector[(String, Int)] = proc => mapFn(proc.data)
      vectorCPU.broadcast(lambda, reduceFn)
      registerPerformanceTestPerformance(vProcQSize, {
        doMR(vectorCPU, shortShakesWordFrequencies)
      })
    }

    override val test: TestEntity = new TestEntity(testId,entity, description)

    override def testId: Int = 6
  }

  // ShortMapReduceWCNarrowcastMethodMeasuredPerformanceTest

  object LongMapReduceWCProgramMethodMeasuredPerformanceTest extends MeasuredPerformanceTest {
    override def entity: String = "MapReduce Long Word Count"

    override def description: String = "long program method of finding word freqs for Complete Works of Shakespeare"

    override def benchmarkTest(): Unit = MapReduceWordCountInfo.nonConcurrentBenchmark(
      longShakesLinesWithIndex, longShakesWordFrequencies, this)

    override def performanceTest(vProcQSize: VProcsQSize): Unit = {
      import MapReduceWordCountInfo.{mapFn, partition, reduceFn}
      val cpu = Master[Vector[(String, Int)]](vProcQSize.nProcessors, vProcQSize.qSize)
      val groupedData = partition(longShakesLinesWithIndex, vProcQSize.nProcessors)
      cpu.program(mapFn, reduceFn, groupedData)
      // measure the performance of the MapReduce Operation
      registerPerformanceTestPerformance(
      vProcQSize, {
        MapReduceWordCountInfo.doMR(cpu, longShakesWordFrequencies)(fiveSecPatience)
      }
      )
    }

    val longShakesLinesWithIndex =
      Source.fromURL(getClass.getResource("/all-shakespeare.txt")).getLines().zipWithIndex.toVector

    val longShakesWordFrequencies = Vector(("thou", 5783), ("thy", 4272), ("shall", 3724), ("thee", 3377), ("lord", 3354), ("king", 3317), ("sir", 3018), ("good", 2903), ("come", 2593), ("love", 2194), ("let", 2174), ("enter", 2081), ("ill", 2045), ("hath", 2031), ("like", 1901), ("man", 1865), ("did", 1745), ("say", 1734), ("make", 1734), ("know", 1713), ("tis", 1430), ("henry", 1312), ("speak", 1191), ("duke", 1167), ("time", 1098), ("tell", 1095), ("doth", 1082), ("exeunt", 1061), ("think", 1055), ("heart", 1049), ("queen", 1002))

    override val test: TestEntity = new TestEntity(testId,entity, description)

    override def testId: Int = 7
  }

  // end LongMapReduceWCProgramMethodMeasuredPerformanceTest

  object MapReduceWordCountInfo {

    def nonConcurrentBenchmark(lines: Vector[(String, Int)], expectedFreqs: Vector[(String, Int)],
                               test: MeasuredPerformanceTest) = {
      var resultOfMap = Vector(("", 0))
      var resultOfReduce = Vector(("", 0))
      // measure the performance of the MapReduce Operation
      test.registerBenchmarkPerformance({
        resultOfMap = mapFn(Vector(lines))
        resultOfReduce = reduceFn(resultOfMap, Vector())
      })
      // verify the results
      expectedFreqs foreach {
        resultOfReduce should contain(_)
      }
      println("Results of " + test.entity + " - " + test.description + ": \n" +
        sortAndFilter(resultOfReduce))
    }

    /**
     * Convenience method to verify the results of either MapReduce test
     * @param cpu The CPU to verify results on
     */
    def doMR(cpu: Master[Vector[(String, Int)]], contains: Vector[(String, Int)])(implicit config: PatienceConfig): Unit = {
      cpu.emit(Future(Vector(("", 0))))
      whenReady(cpu.accept()) {
        result => {
          contains foreach {
            result should contain(_)
          }
        }
      }(config)
    }

    def partition(lines: Vector[(String, Int)], partitions: Int) = {
      val by = math.ceil(lines.length.toDouble / partitions).toInt
      // padTo is necessary so that if we are testing 10 vCPUs on a dataset w/ 9 items, CPU doesn't throw error
      lines.grouped(by).toVector.padTo(partitions,Vector(("",0)))
    }

    /**
     * Extract an iterable of word tokens from the given string.
     * @param str String of words
     * @return List of tokenized words with punctuation removed
     */
    def toWords(str: String) = str.toLowerCase.replaceAll("[^a-zA-Z0-9\\s]", "").split("\\s+")

    /**
     * Produces a collection of strings consisting in all of the tokens found in the supplied collection of strings
     */
    def toWords[Collection1[String] <: Seq[String]](lines: Collection1[String]): Seq[String] = lines flatMap {
      toWords(_)
    }

    /**
     * Tokenizes the input lines on a space and returns the tokenized words in a tuple with a 1 attached
     *
     * example input:
     * ("Being your slave, what should I do but tend", 1) // the number on right doesn't matter
     * ("Upon the hours and times of your desire?"   , 2)
     * ("I have no precious time at all to spend"    , 3)
     *
     * example output:
     * ("Being", 1)
     * ("your", 1)
     * ("slave", 1)
     * ...
     */
    def mapFn(data: Vector[Vector[(String, Int)]]): Vector[(String, Int)] = data.flatMap {
      lines => toWords(lines.unzip._1).map((_, 1))
    }


    /**
     * Gather concatenate two lists, group by their keys, then return the map
     * formed by summing the values where the keys are the same.
     *
     * example input:
     * ("you", 1)
     * ("you", 1)
     * ("you", 1)
     * ("your", 1)
     *
     * example output:
     * ("you", 3)
     * ("your", 1)
     *
     * @return List of tuples of key value pairs where the value represents the frequency of the key
     */
    def reduceFn(resultOfLambda: Vector[(String, Int)], resultOfAccept: Vector[(String, Int)]): Vector[(String, Int)] = {
      // broken out for easy breakpointing / debugging
      val r1 = resultOfLambda ++ resultOfAccept
      val r2 = r1.groupBy { case (line, num) => line }
      val r3 = r2.mapValues({
        _.unzip._2.sum
      })
      val r4 = r3.toVector
      r4
    }

    /**
     * Returns the words that are in the top 1000 that are greater than one letter long
     * and which are not a stopword, sorted in descending order by frequency.
     */
    def sortAndFilter(lines: Vector[(String, Int)]) = {
      lines
        .filter(_._2 > 1000) // top 1000 words
        .filter(_._1.length > 1) // words longer than one letter
        .filterNot { case (str, idx) => stopwords.contains(str) } // remove stopwords
        .sortBy {
        _._2
      } // sort ascending by freq
        .reverse // sort descending by freq
    }

    /**
     * Stopwords used by MySQL for constructing a fulltext search index; used by MapReduce Word Count Test
     * https://dev.mysql.com/doc/refman/5.1/en/fulltext-stopwords.html
     */
    val stopwords = Array("a", "about", "above", "above", "across", "after", "afterwards", "again", "against", "all", "almost", "alone", "along", "already", "also", "although", "always", "am", "among", "amongst", "amoungst", "amount", "an", "and", "another", "any", "anyhow", "anyone", "anything", "anyway", "anywhere", "are", "around", "as", "at", "back", "be", "became", "because", "become", "becomes", "becoming", "been", "before", "beforehand", "behind", "being", "below", "beside", "besides", "between", "beyond", "bill", "both", "bottom", "but", "by", "call", "can", "cannot", "cant", "co", "con", "could", "couldnt", "cry", "de", "describe", "detail", "do", "done", "down", "due", "during", "each", "eg", "eight", "either", "eleven", "else", "elsewhere", "empty", "enough", "etc", "even", "ever", "every", "everyone", "everything", "everywhere", "except", "few", "fifteen", "fify", "fill", "find", "fire", "first", "five", "for", "former", "formerly", "forty", "found", "four", "from", "front", "full", "further", "get", "give", "go", "had", "has", "hasnt", "have", "he", "hence", "her", "here", "hereafter", "hereby", "herein", "hereupon", "hers", "herself", "him", "himself", "his", "how", "however", "hundred", "ie", "if", "in", "inc", "indeed", "interest", "into", "is", "it", "its", "itself", "keep", "last", "latter", "latterly", "least", "less", "ltd", "made", "many", "may", "me", "meanwhile", "might", "mill", "mine", "more", "moreover", "most", "mostly", "move", "much", "must", "my", "myself", "name", "namely", "neither", "never", "nevertheless", "next", "nine", "no", "nobody", "none", "noone", "nor", "not", "nothing", "now", "nowhere", "of", "off", "often", "on", "once", "one", "only", "onto", "or", "other", "others", "otherwise", "our", "ours", "ourselves", "out", "over", "own", "part", "per", "perhaps", "please", "put", "rather", "re", "same", "see", "seem", "seemed", "seeming", "seems", "serious", "several", "she", "should", "show", "side", "since", "sincere", "six", "sixty", "so", "some", "somehow", "someone", "something", "sometime", "sometimes", "somewhere", "still", "such", "system", "take", "ten", "than", "that", "the", "their", "them", "themselves", "then", "thence", "there", "thereafter", "thereby", "therefore", "therein", "thereupon", "these", "they", "thickv", "thin", "third", "this", "those", "though", "three", "through", "throughout", "thru", "thus", "to", "together", "too", "top", "toward", "towards", "twelve", "twenty", "two", "un", "under", "until", "up", "upon", "us", "very", "via", "was", "we", "well", "were", "what", "whatever", "when", "whence", "whenever", "where", "whereafter", "whereas", "whereby", "wherein", "whereupon", "wherever", "whether", "which", "while", "whither", "who", "whoever", "whole", "whom", "whose", "why", "will", "with", "within", "without", "would", "yet", "you", "your", "yours", "yourself", "yourselves", "the")

  }

  // END MapReduceWordCountInfo

}
