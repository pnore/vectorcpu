package org.cscie54.matrix

/**
 * Integer matrix
 * Create a matrix containing integer elements
 * Also provides some utility methods
 *
 * @param rows number of rows for new matrix
 * @param cols number of columns
 * @param fill integer value for elements
 */
class IntMatrix(override val rows: Int, override val cols: Int, val fill: Int) extends Matrix[Int] {
  private val m = Array.tabulate(rows, cols)( (x,y) => fill )

  // Return the matrix itself
  val get = m
  override def cell = m

  /**
   * Add values of another matrix to the values in this matrix
   * and return a new matrix with the sum
   *
   * @param other other matrix whose values we will add to this matrix
   */
  def sum(other: IntMatrix): IntMatrix = {
    checkSize(other)

    val im = new IntMatrix(rows, cols, 0)
    val result: Array[Array[Int]] = im.get
    val matrix2 = other.get

    for (r <- 0 to rows-1; c <- 0 to cols-1) {
      result(r)(c) = m(r)(c) + matrix2(r)(c)
    }

    wrapIntArray(result)
  }


  /**
   * Wrap an integer array in an IntMatrix
   *
   * @param a a 2D array to convert into an integer matrix
   * @return a new IntMatrix instance
   */
  def wrapIntArray(a: Array[Array[Int]]): IntMatrix = {
    val rows = a.length     // Assume array has good dimensions
    val cols = a(0).length

    val result = new IntMatrix(rows, cols, 0)

    // Copy values from input array to matrix
    for (r <- 0 to rows-1; c <- 0 to cols-1) {
      result.cell(r)(c) = a(r)(c)
    }

    result
  }
}
