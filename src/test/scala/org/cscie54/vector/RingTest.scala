package org.cscie54.vector

import akka.actor.ActorSystem
import org.cscie54.shared.{BasicTestKit, ActorBasicTestKit}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.language.postfixOps

class RingTest extends BasicTestKit {

  // -------------| BEGIN PRIMARY TESTS |-------------->

  behavior of "Ring.apply"
  it should "construct independent rings" in {
    val r1 = getRing(nProcessors = nProcessors + 1)
    val r2 = getRing()
    r1 should not be theSameInstanceAs(r2)
    r1.noOfProcessors should be(nProcessors + 1)
    r2.noOfProcessors should be(nProcessors)
  }

  it should "complain about invalid qSizes" in {
    intercept[IllegalArgumentException] {
      val ring = getRing( qSize = -1 )
    }
  }

  "Ring.noOfProcessors" should "correctly read nProcessors processors" in {
    val ring = getRing()
    ring.noOfProcessors should be(nProcessors)
  }

  "Ring.addProcessor" should "add a Processor to the Ring" in {
    val ring = getRing()
    // the Proc trait implemented by Processor calls ring.addProcessor(this)
    new Processor(nProcessors + 1, ring)
    ring.noOfProcessors should be(nProcessors + 1)
  }

  "Ring.addAllProcessors" should "add all Processors to the Ring" in {
    val ring = getRing() // returns ring with processors 1 to nProcessors
    ring.noOfProcessors should be(nProcessors)
    val procs = (nProcessors + 1 to nProcessors + nProcessors) map (new Processor[testType](_, ring).asInstanceOf[Proc[testType]])
    assert(ring.noOfProcessors == 2 * nProcessors)
  }

  /**
   * Currently, we have not implemented any determinism in the API explaining
   * what the behavior should be if you add a Processor and it already exists.
   * For example, if you narrowcast data to a Processor and then replace it,
   * should it still be there? Until we implement these features, the sensible thing
   * to do is to throw an exception.
   */
  it should "complain if you add a Processor that has already been added" in {
    val ring = getRing() // already has nProcessors, so
    // adding a new processor with an existing index should throw an exception
    intercept[IllegalArgumentException] {
      ring.addProcessor(new Processor(nProcessors / 2, ring)) // instantiate new processor
    }
  }

  // unimplemented; see #44
//  behavior of "Ring.removeProcessor"
//  ignore should s"decreases numProcessors to nProcessors - 1" in {
//    val ring = getRing()
//    ring.removeProcessor(Processor(1, ring))
//    ring.noOfProcessors should be(nProcessors - 1)
//  }

  "Ring.{processorEmit,processorFetch}" should "emit and fetch to nearest neighbor, qSize = 2" taggedAs ITest in {
    val qSize = 2 // for ArrayBlockingQueue
    val expected = 42
    val fetchFuture: Future[testType] = simulateFetch(testData = expected, qSize = qSize)
    whenReady(fetchFuture) { fetchResult =>
      assert(fetchResult == expected)
    }
  }

  it should "emit and fetch to nearest neighbor, qSize = 0" taggedAs ITest in {
    val qSize = 0 // for ArrayBlockingQueue
    val expected = 42
    val fetchFuture: Future[testType] = simulateFetch(testData = expected, qSize = qSize)
    whenReady(fetchFuture) { fetchResult =>
      assert(fetchResult == expected)
    }
  }

  // <-------------| END PRIMARY TESTS |---------------
  // --------------| BEGIN DATA MEMBERS |------------->

  type testType = Int

  val nProcessors = 5
  val qSize = 0

  // <-------------| END DATA MEMBERS |-----------------
  // --------------| BEGIN HELPER METHODS |------------>

  def getRing(qSize: Int = qSize, nProcessors: Int = nProcessors) = {
    val ring = Ring[testType, Proc[testType]](qSize)
    val procs = (1 to nProcessors) map (new Processor[testType](_, ring).asInstanceOf[Proc[testType]])
//    ring.addAllProcessors(procs)
    ring
  }

  def getNewProc(ring: Ring[testType,Proc[testType]], nProcessors: Int = nProcessors+1): Processor[testType] = {
    new Processor[testType](nProcessors,ring)
  }

  def simulateFetch(testData: Int = 42, qSize:Int = qSize, numProcessors: Int = nProcessors): Future[testType] = {
    val ring = getRing(qSize, numProcessors)
    val p1 = getNewProc(ring, numProcessors + 1)
    val p2 = getNewProc(ring, numProcessors + 2)
    val p3 = getNewProc(ring, numProcessors + 3)

    // Emit from p2 to p3, tell p3 to fetch
    val emitResult: Unit = Future{ring.processorEmit(p2, Future(testData))}
    ring.processorFetch(p3)
  }

  // <-------------| END HELPER METHODS |---------------

}
