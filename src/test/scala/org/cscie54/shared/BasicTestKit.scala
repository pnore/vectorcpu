package org.cscie54.shared

import java.io.PrintWriter
import java.text.SimpleDateFormat
import java.util.Date

import akka.testkit.TestKit
import org.scalatest._
import org.scalatest.concurrent.ScalaFutures

import scala.language.postfixOps

/**
 * Utility class to extend in test specs for consistency. From Wyatt's book *Akka Concurrency*.
 *
 * @see http://proquest.safaribooksonline.com.ezp-prod1.hul.harvard.edu/book/programming/java/9780981531663/akka-testing/sec_the_testkit_actorsystem_and_scalatest_html?uicode=harvard
 */
trait BasicTestKit extends FlatSpecLike
with Matchers
with ScalaFutures
with BeforeAndAfterAll
with Assertions {
  val date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())

  // # Benchmarking Instructions
  // 0. Open src/main/org/cscie54/shared/BasicTestKit.scala and fill in information at top:

  ///////////// TESTING METRICS: Fill in your info below ////////////////////////

  // 1. ADD A NEW TESTER FOR YOUR COMPUTER
  //  val YourNameHere =  Tester(person = "YourNameHere",
  //                             cores = "YourNumCores",
  //                             processor = "Y.Z GHz Your Processor Name Here" )
  val Myer =      Tester(person= "Myer" ,       cores=  "2" , processor = "2.5 GHz Intel Core i5"  )
  val Harry =     Tester(person= "Harry",       cores=  "2" , processor = "2.5 GHz Intel Core i5"  )
  val Ivan =      Tester(person = "Ivan",       cores = "4" , processor = "2.3 Ghz Intel Core i7"  )

  // 2. ADD YOUR TESTER TO THE CURRENT TESTING SESSION
  val curTestSession = TestSession(Myer, date) // replace name here with your own

  // 3. RUN TESTS AND SEND vector-processor-performance-metrics.txt to Myer at myer.nore@gmail.com
  //    OR INSERT THEM INTO COLUMN `A` AT https://goo.gl/j0UuIi BELOW ALL OTHER ENTRIES.

  ///////////// TESTING METRICS: Fill in your info above ////////////////////////


  object LoadTest extends Tag("Load Test")
  object ITest extends Tag("Integration Test")
  object Benchmark extends Tag("Single Threaded Benchmark")

  case class TestType(tag: Tag) {
    override def toString: String = tag.name
  }

  case class Column(heading: String)

  class TableData(val columns: List[Column]) {
    def heading: String = TableData.heading(columns)
  }
  object TableData {
    var delimiter = "|"
    def heading(columns: List[Column]): String = tablify(columns.map(_.heading)) + delimiter
    def tablify(params: String*): String = {
      tablify(params,delimiter)
    }
    def tablify(params: Seq[String], delim: String): String = params.mkString(delim)
    def tablify(params: List[String]): String = tablify(params, delimiter)
  }

  case class Tester(person: String , cores: String , processor: String )
   extends TableData(List(Column("Person"), Column("Cores"), Column("Processor"))) {
    override def toString: String = { TableData.tablify(person, cores, processor) }
  }

  case object TestSession {
    def saveMetricsToFile(filename: String = "vector-processor-performance-metrics.txt") = {
      Some(new PrintWriter(filename)).foreach{p => p.write(curTestSession.toString); p.close}
    }
    val instructions =
      """
        |  // # Benchmarking Instructions
        |  // 0. Open src/main/org/cscie54/shared/BasicTestKit.scala and fill in information at top:
        |
        |  ///////////// TESTING METRICS: Fill in your info below ////////////////////////
        |
        |  // 1. ADD A NEW TESTER FOR YOUR COMPUTER
        |  //  val YourNameHere =  Tester(person = "YourNameHere",
        |  //                             cores = "YourNumCores",
        |  //                             processor = "Y.Z GHz Your Processor Name Here" )
        |  val Myer =      Tester(person= "Myer" ,       cores=  "2" , processor = "2.5 GHz Intel Core i5"  )
        |  val Harry =     Tester(person= "Harry",       cores=  "2" , processor = "2.5 GHz Intel Core i5"  )
        |  val Ivan =      Tester(person = "Ivan",       cores = "4" , processor = "2.3 Ghz Intel Core i7"  )
        |
        |  // 2. ADD YOUR TESTER TO THE CURRENT TESTING SESSION
        |  val curTestSession = TestSession(Myer, date) // replace name here with your own
        |
        |  // 3. RUN TESTS AND SEND vector-processor-performance-metrics.txt to Myer at myer.nore@gmail.com
        |  //    OR INSERT THEM INTO COLUMN `A` AT https://goo.gl/j0UuIi BELOW ALL OTHER ENTRIES.
        |
        |  ///////////// TESTING METRICS: Fill in your info above ////////////////////////
      """.stripMargin
  }
  case class TestSession( tester: Tester, date: String) extends TableData(tester.columns.toList :+ Column("Date")) {
    var metrics = Vector[Metric]()
    def addMetric(m: Metric) = metrics = metrics :+ m
    override def toString: String = {

//        "\nBENCHMARKING INSTRUCTIONS: \n" +
//        "\nPlease create a new val containing a Tester object in the TESTING METRICS section at the top of " +
//        "\norg.cscie54.shared.BasicTestKit. Next, customize the value of the curTestSession val in that " +
//        "\nsection. Run the test suite with sbt test, then send " +
//        "\nvector-processor-performance-metrics.txt to Myer at ptr.nore@gmail.com or insert them in" +
//        "\ncolumn A of https://docs.google.com/spreadsheets/d/1H9pzWH1w1iIBhf5fghGbZe7bQsEIot6_e-pQRI_38nI/edit#gid=0, beneath all other entires." +
//        "\n Be sure to delete or not include the header rows." +
//        "\n Remember to customize" +
//        "\n Optionally, send Myer Nore your copy of vector-processor-performance-metrics.txt."
      val metricRows = metrics.map{metric => TableData.tablify(tester.toString, date, metric.toString)}.mkString("\n")
      heading + TableData.heading(Metric.columns) + "\n" + metricRows
    }
  }

  case object TestEntity {
    val columns = List(Column("Test Id"), Column("Test Entity"),Column("Test Desc"))
  }
  case class TestEntity(id: Int, entity: String, description: String) extends
  TableData(TestEntity.columns) {
    override def toString: String = {
      TableData.tablify(id.toString, entity, description)
    }
  }

  case object Metric {
    val columns = TestEntity.columns ++ List( Column("Test Type"), Column("Runtime (secs)") )
  }
  case class Metric(entity: TestEntity, testType: TestType, dur: Long)
    extends TableData(Metric.columns) {
    override def toString: String = {
      TableData.tablify(entity.toString, testType.toString, prettyNanoToSecs(dur))
    }
  }



  /**
   * "easier" version of printPerf that doesn't generate metrics for the table as well
   *
   * @param f A function block to execute and mark the time of
   */
  def printPerf(f: => Unit, testEntity: String = ""): Unit = {
    printPerf(f, new TestEntity(-1,testEntity,""), ITest)
  }

  /**
   * Take a statement, and execute a function while measuring its runtime, which is
   * printed out at the conclusion of the function.
   *
   * @param f A function block to execute and mark the time of
   */
  def printPerf(f: => Unit, testEntity: TestEntity, testType: Tag): Unit = {
    val msg = if( testEntity != null && testEntity.entity.length > 0 ) s"\n$testEntity\n" else "Performance "
    // Start timer
    val start = System.nanoTime
    f
    val runtime = System.nanoTime - start
    println(s"\n$msg (secs):  " + prettyNanoToSecs(runtime))
    curTestSession.addMetric(new Metric(testEntity, TestType(testType), runtime))
  }

  /**
   * Create a string version of a decimal number expected
   * to be some number less than 1000 seconds
   *
   * @param n time interval in nanoseconds
   * @return formatted string, time interval in seconds
   */
  def prettyNanoToSecs(n: Double) = {
    val pretty = n / 10e9
    f"$pretty%3.4f"
  }

  override protected def afterAll(): Unit = {
    println(curTestSession)
    println(TestSession.instructions)
//    TestSession.saveMetricsToFile()
    // we want to only do this in ActorBasicTestKit so that we don't overwrite the metrics
  }

}
