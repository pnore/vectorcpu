package org.cscie54.vector

import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import akka.util.Timeout

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Promise, Future}
import scala.language.postfixOps
import scala.reflect.ClassTag
import scala.util.{Try, Success, Failure}

/** Client class - separate file */
case class Master[T:ClassTag](
  noOfProcessors: Int = Runtime.getRuntime.availableProcessors(),
  qSize: Capacity
)(implicit system: ActorSystem) extends MCP[T] {

  if (noOfProcessors <= 0) throw new Exception("CPU must have at least 1 execution core(s)")

  override val ring = Ring[T,Proc[T]](qSize)
  ring.addProcessor(this) // Add Master to Ring

  val processors: IndexedSeq[Processor[T]] =
    for (i <- 1 to noOfProcessors) yield Processor[T](i, ring) // MCP has id of 0

  implicit val t = Timeout(10, TimeUnit.SECONDS)
  val vectorRouter = system.actorOf(VectorRouter.props(processors.toVector))

  override def narrowcast(index: Int, data: Vector[T]): Unit = {
    if (index <= 0 || index > noOfProcessors)
      throw new Exception(s"$index out of range")
    if (data == null)
      throw new Exception("Data cannot be null")
    vectorRouter ! Narrowcast(index, data)
  }

  override def broadcast(lmbd: Processor[T] => T, fn: (T, T) => T): Unit = {
    val lambda: Processor[T] => Unit =
      proc => {
        val accumulatedResult: Future[T] = {
          Try { lmbd(proc) } match {
            case Success(res) => proc.accept map { acc => fn(acc, res) }
            case Failure(e) => proc.accept map { _ => throw e }
          }
        }
        proc.emit(accumulatedResult)
      }
    vectorRouter ! Broadcast(lambda)
  }

  override def program(lmbd: (Vector[T]) => T, fn: (T, T) => T, data: Vector[T]): Unit = {
    if (data.size < noOfProcessors) throw new Exception("fewer data elements than # of cores")

    val by = math.ceil (data.length.toDouble / noOfProcessors) toInt
    val groupedData: Vector[Vector[T]] = (data toVector) grouped(by) toVector

    def alignData(data: Vector[Vector[T]]): Vector[Vector[T]] =
      if (data.length < noOfProcessors) alignData(data :+ Vector[T]()) else data

    val tuples: Vector[(Vector[T], Int)] = alignData(groupedData) zip (Stream from 1)
    for ((subArr, idx) <- tuples) {
      val lambda: Processor[T] => Unit =
        proc => {
          val accumulatedResult: Future[T] = {
            Try { lmbd(subArr) } match {
              case Success(res) => proc.accept map { acc => fn(acc, res) }
              case Failure(e) => proc.accept map { _ => throw e }
            }
          }
          proc.emit(accumulatedResult)
        }
      vectorRouter ! Program(idx, lambda)
    }
  }

}

case class Processor[T:ClassTag](
  override val index: Int,
  override val ring: Ring[T,Proc[T]]
) extends Proc[T] {

  var data = Vector[T]()
  ring.addProcessor(this)

}

