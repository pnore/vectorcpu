package org.cscie54.vector

import java.util
import java.util.Comparator
import java.util.concurrent.{ArrayBlockingQueue, BlockingQueue, SynchronousQueue}

import scala.concurrent.Future

/**
 * A Ring data structure which provides transparent, processor agnostic
 * message passing with neighbouring processors
 *
 * @tparam T        Data type for the Ring; each Proc must have the same data type
 * @tparam GenProc  Generic Processor that is subclass of Proc[T], where T is the underlying datatype
 */
trait Ring[T, GenProc <: Proc[T]] {


  /**
   * Adds the processor to the TreeMap so that it can participate in the
   * computation and create the communication blocking queue for the processor from which
   * it reads the incoming computation result
   * If the queue size is defined as 0(zero) then we use SynchronousQueue as there is no buffer
   * and it works similar to a pipe in unix
   *
   * @param processor Processor to be added to the ring
   */
  def addProcessor(processor: GenProc): Unit

  /**
   * Adds the collection of processors to the processorMap so that they start working on the
   * computation
   *
   * @param processors collection of processors in an array
   */
  def addAllProcessors(processors: IndexedSeq[GenProc]): Unit

  /**
   * Adds to processors Queue, used by emit function
   *
   * @param processor Processor to whose queue we need to write
   * @param data Value to be added to the processors queue
   */
  def processorEmit(processor: GenProc, data: Future[T])

  /**
   * Remove the processor from the Ring, this function is not used currently and is designed for
   * the future to simulate actual processors that can be added and removed dynamically at runtime.
   * Because of the circular nature of ring, similar to ConsistentHash the system will execute as
   * desired and the pointers will be updated automatically.
   *
   * UNIMPLEMENTED
   *
   * @param processor Processor to be removed from the ring
   **/
  def removeProcessor(processor: GenProc) = ???

  /**
   * Fetches the result from processors blocking queue
   *
   * @param processor Processor to which we need to fetch the results
   * @return Computed result from the head of the queue
   */
  def processorFetch(processor: GenProc): Future[T]

  def noOfProcessors: Int
}

/**
 * Companion object for Ring, which is used to instantiate new Rings.
 */
object Ring {

  /**
   * Instantiates a new Ring given zero or more Processors.
   * @tparam T        Data
   * @tparam GenProc  Generic Processor that is subclass of Proc[T], where T is the underlying datatype
   * @return  Instantiated Ring with Processors added
   */
  def apply[T, GenProc <: Proc[T]](qSize: Int): Ring[T,GenProc] = new RingImpl[T,GenProc](qSize)

  private class RingImpl[T, GenProc <: Proc[T]](private val qSize: Capacity) extends Ring[T,GenProc] {

    require(qSize >= 0, "qSize must be greater than or equal to zero")

    /**
     * Keep the map sorted based on the processor id. MCP is the first processor that is added
     * to system and is the first element in the sorted map
     */
    private val processorMap = new util.TreeMap[GenProc, BlockingQueue[Future[T]]](
      new Comparator[GenProc] {
        override def compare(o1: GenProc, o2: GenProc): Int = o1.index.compareTo(o2.index)
      }
    )

    /** MCP should have index 0 so that its the first element in the ring */
    private def mcpQueue: BlockingQueue[Future[T]] = processorMap.firstEntry().getValue

    /** Put is blocking */
    private def mcpQueueAdd(data: Future[T]) = mcpQueue.put(data)

    /** Get processor queue */
    private def getQueue(processor: GenProc): BlockingQueue[Future[T]] = processorMap.get(processor)

    override def addProcessor(processor: GenProc): Unit = {
      // TODO: #45 Ring.addProc dupe should throw exception OR implement defined behavior
      if(processorMap.containsKey(processor)) throw
        new IllegalArgumentException(s"processor $processor has already been added to the ring")
        if (qSize == 0)
          processorMap.put(processor, new SynchronousQueue[Future[T]]())
        else
          processorMap.put(processor, new ArrayBlockingQueue[Future[T]](qSize))
    }

    override def addAllProcessors(processors: IndexedSeq[GenProc]): Unit =
      for (i <- 0 until processors.length) addProcessor(processors(i))

//    TODO: make Ring responsible for processor index
//    override def getIndex(processor: GenProc): Int = processorMap


    /**
     * Finds the processors nearest neighbour and then adds the value to the queue of the
     * nearest neighbour, if its the last element then it means the result was computed
     * successfully and the result needs to be added to the MCPs queue which completes the
     * circular chain
     *
     * @param processor Processor whose neighbour we need to add data
     * @param data Result from the computation that needs to be pushed to the next processor
     */
    private def addDataToNearestNeighbour(processor: GenProc, data: Future[T]): Unit = {
      // Get the ceiling of processor that is the next closest processor
      val proc = processorMap.higherEntry(processor)
      // If processor is null then means that it is the last element in the sortedMap, so the
      // final result should be added to the queue of mcp from which mcp will fetch the result
      if (null == proc) {
        mcpQueueAdd(data)
      } else {
        // Block and add data
        proc.getValue.put(data)
      }
    }

    /**
     * Adds to processors Queue, used by emit function
     *
     * @param processor Processor to whose queue we need to write
     * @param data Value to be added to the processors queue
     */
    override def processorEmit(processor: GenProc, data: Future[T]) = addDataToNearestNeighbour(processor, data)

    /**
     * Fetches the result from processors blocking queue
     *
     * @param processor Processor to which we need to fetch the results
     * @return Computed result from the head of the queue
     */
    override def processorFetch(processor: GenProc): Future[T] = getQueue(processor).take()

    override def noOfProcessors = processorMap.size()

  }

}
