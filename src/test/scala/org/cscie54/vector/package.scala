package org.cscie54

import org.scalatest.Tag

case class VProcsQSize(nProcessors: Int, qSize: Int) extends Tag(s"$nProcessors VProcessors, qSize $qSize")

object Defaults {

  val allVProcQSizes = Array(

    // qSize = 5
    VProcs2qSize5
    ,
    VProcs4qSize5
    ,
    VProcs6qSize5
    ,
    VProcs8qSize5

    // qSize = 3
    ,
    VProcs2qSize3
    ,
    VProcs4qSize3
    ,
    VProcs6qSize3
    ,
    VProcs8qSize3

    // qSize = 1
    ,
    VProcs2qSize1
    ,
    VProcs4qSize1
    ,
    VProcs6qSize1
    ,
    VProcs8qSize1

    // currently the tests are halting on qSize = 0

    // qSize = 0
    ,
    VProcs2qSize0
    ,
    VProcs4qSize0
    ,
    VProcs6qSize0
    ,
    VProcs8qSize0

  )

  val nProcessors = 2
  val qSize = 1

}

object VProcs2qSize0 extends VProcsQSize(2, 0)

object VProcs4qSize0 extends VProcsQSize(4, 0)

object VProcs6qSize0 extends VProcsQSize(6, 0)

object VProcs8qSize0 extends VProcsQSize(8, 0)


object VProcs2qSize1 extends VProcsQSize(2, 1)

object VProcs4qSize1 extends VProcsQSize(4, 1)

object VProcs6qSize1 extends VProcsQSize(6, 1)

object VProcs8qSize1 extends VProcsQSize(8, 1)


object VProcs2qSize3 extends VProcsQSize(2, 3)

object VProcs4qSize3 extends VProcsQSize(4, 3)

object VProcs6qSize3 extends VProcsQSize(6, 3)

object VProcs8qSize3 extends VProcsQSize(8, 3)


object VProcs2qSize5 extends VProcsQSize(2, 5)

object VProcs4qSize5 extends VProcsQSize(4, 5)

object VProcs6qSize5 extends VProcsQSize(6, 5)

object VProcs8qSize5 extends VProcsQSize(8, 5)