package org.cscie54.shared

import akka.actor.ActorSystem
import akka.testkit.TestKit
import org.scalatest.concurrent.PatienceConfiguration.Timeout
import org.scalatest.time.{Millis, Seconds, Span}
import scala.concurrent.ExecutionContext.Implicits.global


class ActorBasicTestKit(actorSystem: ActorSystem) extends TestKit(actorSystem) with BasicTestKit {

  override protected def afterAll(): Unit = {
    println(curTestSession.toString)
    println(TestSession.instructions)
    TestSession.saveMetricsToFile()
    TestKit.shutdownActorSystem(system)
  }

  val fiveSecPatience = PatienceConfig(timeout = Span(5, Seconds), interval = Span(500, Millis))
  val tenSecPatience = PatienceConfig(timeout = Span(10, Seconds), interval = Span(100, Millis))
}
