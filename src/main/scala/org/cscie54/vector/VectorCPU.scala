package org.cscie54.vector

import scala.concurrent.Future
import scala.language.postfixOps

/** Trait exposed to the client */
trait Proc[A] {

  /** Returns the index of the proc in the system */
  def index: Int = 0

  /**
   * Supplies the initial value of the computation to start it
   *
   * @param v Value to be provided to the processor to start the computation
   */
  def emit(v: Future[A]): Unit = ring.processorEmit(this, v)

  /**
   * Gets the return of the computation
   *
   * @return result of the previous computation
   */
  def accept(): Future[A] = ring.processorFetch(this)

  /**
   * Gets or sets the ring for this processor, required for emit and accept
   * @return Ring for this Proc
   */
  def ring: Ring[A,Proc[A]]

}

/** Trait exposed to the client */
trait MCP[T] extends Proc[T] {

  /**
   * Narrowcast data to a specific proc at #index
   *
   * @param index index of the proc
   * @param data to load into proc's registers
   */
  def narrowcast(index: Int, data: Vector[T]): Unit

  /**
   * Broadcasts lmbd without data to all procs after narrowcasting
   *
   * @param lmbd sequence of instructions to perform of type () => T
   * @param fn operation to perform on the result of lmdb and accepted value
   */
  def broadcast(lmbd: Processor[T] => T, fn: (T, T) => T): Unit

  /**
   * Distributes data evenly between procs to perform lmdb computation
   *
   * @param lmbd sequence of instructions to perform of type Array[T] => T
   * @param fn operation to perform on the result of lmdb and accepted value
   * @param data to distribute evenly between procs
   */
  def program(lmbd: (Vector[T]) => T, fn: (T, T) => T, data: Vector[T]): Unit
}

