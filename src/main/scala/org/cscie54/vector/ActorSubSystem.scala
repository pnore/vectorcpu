package org.cscie54.vector

import akka.actor.{Actor, ActorLogging, Props, Terminated}
import akka.routing.{ActorRefRoutee, BroadcastRoutingLogic, Router}

/**
 * The companion object for VectorRouter serves as a factory for its Props class instantiation.
 */
object VectorRouter {
  def props[T](procs: Vector[Processor[T]]) = Props(classOf[VectorRouter[T]], procs)
}

/**
 * A VectorRouter is an Actor that routes Broadcast, Narrowcast and Program messages
 * a Vector of Processors.
 *
 * @param procs The Processors to route messages to
 * @tparam T  The type used in calculations
 */
class VectorRouter[T](val procs: Vector[Processor[T]]) extends Actor with ActorLogging {
  val procList = procs map (p => context.actorOf(ProcActor.props(p)))

  val router = {
    val routees = procList map {
      ap => {
        context watch ap
        ActorRefRoutee(ap)
      }
    }
    Router(BroadcastRoutingLogic(), routees)
  }

  def receive = {
    case Broadcast(lambda) => router route(ProcProgram(lambda), sender())
    case Narrowcast(idx, data) => procList(idx-1) ! ProcData(data)
    case Program(idx, lambda) => procList(idx-1) ! ProcProgram(lambda)
    case Terminated(a) => throw new Exception("some processor failed")
  }
}

/**
 * The companion object for ProcActor serves as a factory for its Props class instantiation.
 */
object ProcActor {
  def props[T](proc: Processor[T]) = Props(classOf[ProcActor[T]], proc)
}

/**
 * ProcActor responds to ProcProgram and ProcData messages. ProcProgram programs this
 * actor to compute a specified lambda. ProcData stores the supplied data in this actor
 * for later computation.
 *
 * @param proc  The Processor that this Actor will defer computation to
 * @tparam T    The underlying data type for computation
 */
class ProcActor[T](val proc: Processor[T]) extends Actor with ActorLogging {
  def receive = {
    case ProcProgram(lambda) => lambda.asInstanceOf[Processor[T] => Unit](proc)
    case ProcData(data) => proc.data = data.asInstanceOf[Vector[T]]
  }
}

sealed trait RouterMsg[T]
case class Broadcast[T](lambda: Processor[T] => Unit) extends RouterMsg[T]
case class Program[T](idx: Int, lambda: Processor[T] => Unit) extends RouterMsg[T]
case class Narrowcast[T](idx: Int, data: Vector[T]) extends RouterMsg[T]

sealed trait ProcActorMsg[T]
case class ProcData[T](data: Vector[T]) extends ProcActorMsg[T]
case class ProcProgram[T](lambda: Processor[T] => Unit) extends ProcActorMsg[T]
