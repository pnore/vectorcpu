package org.cscie54

/**
 * Package object for matrix operations
 *
 */
package object matrix {
  // For generic matrix code here to implement "add" etc., see
  // http://typelevel.org/blog/2013/07/07/generic-numeric-programming.html

  /**
   * We use a context bound to require that A has an Addable instance.
   *
   * @tparam A
   */
  trait Addable[A] {
    // Both arguments must be provided. Addable works with the type A, but
    // does not extend it.
    def plus(x: A, y: A): A
  }

  /**
   * This class adds the + operator to any type A that is Addable,
   * by delegating to that Addable's 'plus' method.
   *
   * @param lhs left-hand side of add operation
   * @param ev connect this class with Addable
   * @tparam A variable or object type
   */
  implicit class AddableOps[A](lhs: A)(implicit ev: Addable[A]) {
    def +(rhs: A): A = ev.plus(lhs, rhs)
  }

  /**
   * We use a context bound to require that A has an Addable instance.
   *
   * @param x first operand
   * @param y second operand
   * @tparam A array type
   * @return sum of two matrices
   */
  def add[A: Addable](x: A, y: A): A = x + y

  /**
   * Implicit object for IntMatrix
   */
  implicit object IntMatrixIsAddable extends Addable[IntMatrix] {
    //def plus(x: IntMatrix, y: IntMatrix): IntMatrix = x + y
    def plus(x: IntMatrix, y: IntMatrix): IntMatrix = x.sum(y)
  }

  /**
   * Implicit object for StringMatrix
   */
  implicit object StringMatrixIsAddable extends Addable[StringMatrix] {
    def plus(x: StringMatrix, y: StringMatrix): StringMatrix = x.sum(y)
  }


  // --- Utility methods -----------------------------------------------------

  /**
   * Print a 2D array
   *
   * @param a the 2D array
   * @tparam A array element type
   */
  def printArray2D[A](a: Array[Array[A]]): Unit = {
    val rows = a.length
    val cols = a(0).length

    for (r <- 0 to rows-1) {
      for (c <- 0 to cols-1) {
        print(a(r)(c) + " | ")
      }
      println("")
    }
  }
}
