package org.cscie54.vector

import akka.actor.ActorSystem
import org.cscie54.Defaults._
import org.cscie54.matrix._
import org.cscie54.shared.ActorBasicTestKit
import org.scalatest.time.{Millis, Seconds, Span}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.language.postfixOps

class MasterTest extends ActorBasicTestKit(ActorSystem("MasterTest")) {

  "Master.constructor" should "have ring with nProcessors + 1 processors" in {
    // we have to add one because the MCP added by the Master counts as a Processor
    val master = Master[Int](noOfProcessors = nProcessors, qSize = qSize)
    master.ring.noOfProcessors should be(nProcessors + 1)
  }

  "Master.broadcast" should "begin computation on data in previous narrowcast" taggedAs ITest in {
    val nProcessors = 4
    val nQueueSize = 0

    val data2 = 2 to 4 toVector
    val data3 = 30 to 50 toVector

    val expectedSum = data2.sum + data3.sum

    val lambda: Processor[Int] => Int =
      proc => {
        var tally = 0
        for (x <- proc.data) tally += x
        tally
      }

    val cpu = Master[Int](nProcessors, nQueueSize)

    cpu.narrowcast(2, data2)
    cpu.narrowcast(3, data3)

    cpu.broadcast(lambda, _ + _)

    printPerf({
      cpu.emit(Future(0))
      whenReady(cpu.accept()) {
        _ should be(expectedSum)
      }
    }, "\"Master.{broadcast,narrowcast}\" should \"sum ints in two Vectors\"")
  }

  it should "throw an exceptions if < 0 processors" taggedAs ITest in {
    intercept[Exception] {
      val cpu = Master[Int](-1, qSize)
    }
  }

  "Master.narrowcast" should "distribute different data to different Procs" taggedAs ITest in {
    val nProcessors = 4
    val nQueueSize = 0

    val data2 = 2 to 4 toVector
    val data3 = 30 to 50 toVector

    val expectedSum = data2.sum + data3.sum

    val lambda: Processor[Int] => Int =
      proc => {
        var tally = 0
        for (x <- proc.data) tally += x
        tally
      }

    val cpu = Master[Int](nProcessors, nQueueSize)

    cpu.narrowcast(2, data2)
    cpu.narrowcast(3, data3)
  }

  it should "throw an exception if no data is supplied" taggedAs ITest in {
    val data1: Vector[Int] = null
    val cpu = Master[Int](nProcessors, qSize)

    intercept[Exception] {
      cpu.narrowcast(1, data1)
    }
  }

  it should "throw an exception if index is invalid" taggedAs ITest in {
    val data9 = 0 to 9 toVector
    val cpu = Master[Int](nProcessors, qSize)

    intercept[Exception] {
      cpu.narrowcast(9, data9)
    }
  }

  "Master.program" should "sum ints in an array" taggedAs ITest in {
    val data = 0 to 9 toVector

    val lambda: Vector[Int] => Int =
      data => {
        var tally = 0
        for (x <- data) tally += x
        tally
      }

    val cpu = Master[Int](nProcessors, qSize)
    cpu.program(lambda, _ + _, data)
    printPerf({
      cpu.emit(Future(0))
      whenReady(cpu.accept()) {
        _ should be(data sum)
      }
    }, "\"Master.program\" should \"sum ints in an array\"")
  }

  it should "multiply integers in an array, as in reduceLeft" taggedAs ITest in {
    val data = 1 to 9 toVector

    val lambda: Vector[Int] => Int =
      data => {
        var tally = 1
        for (x <- data) tally *= x
        tally
      }

    val cpu = Master[Int](nProcessors, qSize)

    cpu.program(lambda, _ * _, data)
    printPerf({
      cpu.emit(Future(1))
      whenReady(cpu.accept()) {
        _ should be(data product)
      }
    }, "Master.program should multiply integers in an array, as in reduceLeft")
  }

  it should "add StringMatrices" taggedAs ITest in {
    //val nProcessors = 10    // TODO need new test to verify exception for nProcs > # of data elements

    val nProcessors = 2 // nProcessors = 2 works for example of input array of 2 matrices
    val nQueueSize = 1

    val smBlank = new StringMatrix(4, 4, "")

    // String matrix, size 4x4, filled with "a"
    val sm1 = new StringMatrix(4, 4, "a")

    // Same dimensions as above, but different values
    val sm2 = new StringMatrix(4, 4, "b")

    // Expected matrix representing sum
    val smSumExpected: StringMatrix = new StringMatrix(4, 4, "ab")

    val data: Vector[StringMatrix] = Vector(sm1, sm2)

    val lambda: Vector[StringMatrix] => StringMatrix =
      data => {
        var tally = smBlank
        for (x <- data) tally += x
        tally
      }

    val cpu = Master[StringMatrix](nProcessors, nQueueSize)

    cpu.program(lambda, _ + _, data)
    printPerf({
      cpu.emit(Future(smBlank))

      whenReady(cpu.accept()) { result =>
        assert(result.equals(smSumExpected))
        result.printMatrix()
      }
    }, "\"Master.program\" should \"add StringMatrices\"")
  }

  // TODO: implement #15, Propagate failure exception to client
  it should "handle an exception in the lambda in a predictable way" in {
    val data = 0 to 9 toVector

    val lambda: Vector[Int] => Int =
      data => {
        var tally = 0
        for (x <- data) {
          if (x == 5) throw new IllegalStateException("Simulated Failure")
          tally += 1
        }
        tally
      }

    val cpu = Master[Int](data.length, qSize)
    println("before program")
    cpu.program(lambda, _ + _, data)
    println("before emit")
    cpu.emit(Future(0))

    println("before accept")

    val f = cpu.accept()

    println("before whenReady")

    intercept[IllegalStateException] {
      whenReady(f.failed) {
        throw _
      }(tenSecPatience)
    }
    /* OR you can do just this, not sure why callbacks were failing
    whenReady(f.failed) {
      result =>println(s"Lambda failed!")
    }(PatienceConfig(timeout = Span(10, Seconds), interval = Span(100, Millis)))
    */
  }


  "Master.{broadcast,narrowcast}" should "sum ints in an array" taggedAs ITest in {
    val data = 0 to 9 toVector

    /* the client *has* to know about Processor with this scheme */
    val lambda: Processor[Int] => Int =
      proc => {
        var tally = 0
        for (x <- proc.data) tally += x
        tally
      }

    val cpu = Master[Int](nProcessors, qSize)

    /* this logic is not correct, but we're not using this interface anyway */
    val by = math.ceil(data.length.toDouble / nProcessors) toInt
    val tuples: Vector[(Vector[Int], Int)] = data.grouped(by).toVector zip (Stream from 1)

    for ((d, i) <- tuples) cpu.narrowcast(i, d)
    cpu.broadcast(lambda, _ + _)

    printPerf({
      cpu.emit(Future(0))
      whenReady(cpu.accept()) {
        _ should be(data sum)
      }
    }, "\"Master.{broadcast,narrowcast}\" should \"sum ints in an array\"")
  }

}
