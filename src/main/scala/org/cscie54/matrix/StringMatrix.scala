package org.cscie54.matrix

/**
 * Matrix containing strings
 * @param rows  the number of rows
 * @param cols  the number of cols
 * @param fill  the string to fill the matrix with
 */
class StringMatrix (override val rows: Int, override val cols: Int, val fill: String) extends Matrix[String] {
  private val m = Array.tabulate(rows, cols)( (x,y) => fill )

  // Return the matrix itself
  val get = m
  override def cell = m

  // Add values of another matrix to the values in this matrix
  // and return a new matrix with the sum
  def sum(other: StringMatrix): StringMatrix = {
    checkSize(other)

    val im = new StringMatrix(rows, cols, "")
    val result: Array[Array[String]] = im.get
    val matrix2 = other.get

    for (r <- 0 to rows-1; c <- 0 to cols-1) {
      result(r)(c) = m(r)(c) + matrix2(r)(c)
    }

    wrapStringArray(result)
  }


  // Wrap a String array in a StringMatrix
  def wrapStringArray(a: Array[Array[String]]): StringMatrix = {
    val rows = a.length     // Assume array has good dimensions
    val cols = a(0).length

    val result = new StringMatrix(rows, cols, "")

    // Copy values from input array to matrix
    for (r <- 0 to rows-1; c <- 0 to cols-1) {
      result.cell(r)(c) = a(r)(c)
    }

    result
  }

}
