package org.cscie54.matrix

import org.cscie54.shared.BasicTestKit
import org.scalatest._

/**
 * Test StringMatrix classes, demonstrates using "add" method and plus sign "+"
 * operator overloading for matrix addition.
 * Does not use VectorCPU.
 *
 */
class StringMatrixTest extends BasicTestKit {

  "StringMatrix.{+,add}" should "add two string matrices together" in {
    // String matrix, size 4x4, filled with "a"
    val sm2 = new StringMatrix(4, 4, "a")

    // Same dimensions as above, but different values
    val sm3 = new StringMatrix(4, 4, "b")

    // Expected matrix representing sum
    val smSumExpected: StringMatrix = new StringMatrix(4, 4, "ab")

    // Add two matrices together to create new one using "add" method
    val smSum = add(sm2, sm3)

    // Make sure matrix with sum matched expected values
    assert(smSum.equals(smSumExpected))
    //smSum.printMatrix()

    // Add two matrices together to create new one using "+" operator
    //println("Using + operator")
    val smSum2: StringMatrix = sm2 + sm3
    assert(smSum2.equals(smSumExpected))
    //smSum2.printMatrix()
  }
}
